
import re, nltk, pickle, argparse
import os, sys
import data_helper
import word2vec_extractor
from features import get_features_category_tuples, get_features_from_texts, get_word_embedding_features


from sklearn import svm
from sklearn.naive_bayes import GaussianNB, BernoulliNB
import sklearn, numpy
import pandas as pd
import random
from sklearn import tree
from nltk.classify import SklearnClassifier




DATA_DIR = "data"

random.seed(10)


MODEL_DIR = "models/"
OUTPUT_DIR = "output/"
FEATURES_DIR = "features/"

def write_features_category(features_category_tuples, output_file_name):
    output_file = open("{}-features.txt".format(output_file_name), "w", encoding="utf-8")
    for (features, category) in features_category_tuples:
        output_file.write("{0:<10s}\t{1}\n".format(category, features))
    output_file.close()


def get_classifier(classifier_fname):
    classifier_file = open(classifier_fname, 'rb')
    classifier = pickle.load(classifier_file)
    classifier_file.close()
    return classifier


def save_classifier(classifier, classifier_fname):
    classifier_file = open(classifier_fname, 'wb')
    pickle.dump(classifier, classifier_file)
    classifier_file.close()
    info_file = open(classifier_fname.split(".")[0] + '-informative-features.txt', 'w', encoding="utf-8")
    for feature, n in classifier.most_informative_features(100):
        info_file.write("{0}\n".format(feature))
    info_file.close()
    
def evaluate(classifier, features_category_tuples, reference_text, data_set_name=None):
    print("Evaluating...")
    # test on the data
    accuracy = nltk.classify.accuracy(classifier, features_category_tuples)


    accuracy_results_file = open("{}_results.txt".format(data_set_name), 'w', encoding='utf-8')
    accuracy_results_file.write('Results of {}:\n\n'.format(data_set_name))
    accuracy_results_file.write("{0:10s} {1:8.5f}\n\n".format("Accuracy", accuracy))

    features_only = []
    reference_labels = []
    for feature_vectors, category in features_category_tuples:
        features_only.append(feature_vectors)
        reference_labels.append(category)

    predicted_labels = classifier.classify_many(features_only)

    confusion_matrix = nltk.ConfusionMatrix(reference_labels, predicted_labels)

    accuracy_results_file.write(str(confusion_matrix))
    accuracy_results_file.write('\n\n')
    accuracy_results_file.close()

    predict_results_file = open("{}_output.txt".format(data_set_name), 'w', encoding='utf-8')
    for reference, predicted, text in zip(
            reference_labels,
            predicted_labels,
            reference_text
    ):
        if reference != predicted:
            predict_results_file.write("{0} {1}\n{2}\n\n".format(reference, predicted, text))
    predict_results_file.close()

    return accuracy, confusion_matrix
    
def build_features(data_file, feat_name, save_feats=None):
    # read text data
    positive_texts, negative_texts = data_helper.get_reviews(os.path.join(DATA_DIR, data_file))
    category_texts = {"positive": positive_texts, "negative": negative_texts}
    # build features
    features_category_tuples, texts = get_features_category_tuples(category_texts, feat_name)
    # save features to file
    if save_feats is not None:
        write_features_category(features_category_tuples, save_feats)

    return features_category_tuples, texts
    

def train_with_feature_selection(datafile, feature_set, split_name, no_output = False, save_model=None, save_feats=None):
    print("Identifying best features...")
    train_data = "train_examples.tsv"
    
    selected_features = None
    eval_data = datafile
    
    features_data, texts = build_features(train_data, feature_set) #will train the classifier with these features
    eval_data, eval_texts = build_features(eval_data, feature_set) #will be evaluated against these features (can be specified)
    classifier = nltk.classify.NaiveBayesClassifier.train(features_data)
    
    best = (0.0, 0)
    best_features = classifier.most_informative_features(10000)
    
    most_acc = None
    for i in [2**i for i in range(5, 15)]:
        temp_list = []
        selected_features = set([fname for fname, value in best_features[:i]])
        for feature_dict in features_data:
            temp_dict = {}
            for key in feature_dict[0]:
                if key in selected_features:
                    temp_dict[key] = feature_dict[0][key]
            if temp_dict:
                temp_list.append((temp_dict,feature_dict[1]))
        classifier = nltk.NaiveBayesClassifier.train(temp_list)
        accuracy = nltk.classify.accuracy(classifier, eval_data)
        print("{0:6d} {1:8.5f}".format(i, accuracy))
        if accuracy > best[0]:
            best = (accuracy, i)
            most_acc = temp_list
    
    classifier = nltk.NaiveBayesClassifier.train(most_acc)
    accuracy = nltk.classify.accuracy(classifier, eval_data)
    print("{0:6s} {1:8.5f}".format("Best", accuracy))
    
    pickle.dump(most_acc, open('most_informative.dump', 'wb'))

    return classifier
    

def train_model(datafile, feature_set, split_name, save_model=None, save_feats=None, binning=False):

    features_data, texts = build_features(datafile, feature_set)
    classifier = nltk.classify.NaiveBayesClassifier.train(features_data)

    if save_model is not None:
        save_classifier(classifier, save_model)
    return classifier
    
def train_eval(train_file, eval_file, feature_set, basic = False):
    
    # train the model
    split_name = "train"
    if not basic:
        model = train_with_feature_selection(eval_file, feature_set, split_name)
    else:
        model = train_model(train_file, feature_set, split_name)
    model.show_most_informative_features(20)

    # evaluate the model
    if model is None:
        model = get_classifier(classifier_fname)

    features_data, texts = build_features(eval_file, feature_set)

    if not basic:
        features_data = pickle.load(open('most_informative.dump', 'rb'))

    accuracy, cm = evaluate(model, features_data, texts, data_set_name="eval-{}".format(feature_set))
    print("\nThe best accuracy of {} is: {}".format(eval_file, accuracy))
    print("Confusion Matrix:")
    print(str(cm))

    return accuracy


def predict_main(review_file, pred_file):
    # train the model
    train_file = "train_examples.tsv"
    feature_set = "word_features"
    split_name = "train"
    print("  * training the model with {}".format(feature_set))
    model = train_model(train_file, feature_set, split_name)

    print("  * calculating test set predictions and saving to {}".format(pred_file))
    test = data_helper.get_reviews(review_file)
    feats = get_features_from_texts(test, feature_set)
    preds = []
    with open(pred_file, "w") as fout:
        for feat in feats:
            pred = model.classify(feat)
            preds.append(model.classify(feat))
            #fout.write("{}\n".format(pred))
            #fout.write("{}\n".format(pred))
        for idx,review in enumerate(test):
            fout.write(preds[idx] + "\t" + review + "\n")
            
def train_main():

    train_data = "train_examples.tsv"
    eval_data = "dev_examples.tsv"
    results = []

    feat_set = [
        "word_features", "word_pos_features",
        "word_pos_liwc_features",
        "only_liwc"
    ]
    for feat_set in feat_set:
        print("\n" + "-"*50)
        print("Training with {} \n".format(feat_set))
        acc = train_eval(train_data, eval_data, feat_set, True)

        results.append({
            "features": feat_set,
            "accuracy": acc,
        })

    import pandas as pd
    df = pd.DataFrame(results)
    df.to_csv("results.csv", index=False)




def build_classifier(classifier_name):#string indicates what type of object to return
    """
    Accepted names: nb, dt, svm, sk_nb, sk_dt, sk_svm

    svm and sk_svm will return the same type of classifier.

    :param classifier_name:
    :return:
    """
    if classifier_name == "nb":
        cls = nltk.classify.NaiveBayesClassifier
    elif classifier_name == "nb_sk":#sckikt learn classifier
        cls = SklearnClassifier(BernoulliNB())
    elif classifier_name == "dt":#decision tree
        cls = nltk.classify.DecisionTreeClassifier
    elif classifier_name == "dt_sk":
        cls = SklearnClassifier(tree.DecisionTreeClassifier())
    elif classifier_name == "svm_sk" or classifier_name == "svm":
        cls = SklearnClassifier(svm.SVC())
    else:
        assert False, "unknown classifier name:{}; known names: nb, dt, svm, nb_sk, dt_sk, svm_sk".format(classifier_name)

    return cls


def main():

    all_feature_sets = [
        "word_pos_features", "word_features", "word_pos_liwc_features",
        #"word_embedding",
        #"liwc_features",
        #"binning_word_pos_features",
        #"binning_word_features", "binning_word_pos_liwc_features"
    ]
    
def train_all(eval_data=None, type=None):
    
    features_data, texts = build_features("dev_examples.tsv", "word_features")
    features_data_2, texts2 = build_features("test_examples.tsv", "word_features")
    train_data = pickle.load(open('most_informative.dump', 'rb'))
    
    dev_features_only = []
    dev_reference_labels = []
    for feature_vectors, category in features_data:
        dev_features_only.append(feature_vectors)
        dev_reference_labels.append(category)

        
        
    test_features_only = []
    test_reference_labels = []
    for feature_vectors, category in features_data_2:
        test_features_only.append(feature_vectors)
        test_reference_labels.append(category)

    
        
    
        
    print("Training NB...")
    nb = nltk.NaiveBayesClassifier.train(train_data)
    nb_dev_accuracy = nltk.classify.accuracy(nb, features_data)
    nb_train_accuracy = nltk.classify.accuracy(nb, features_data_2)
    print("NB accuracy dev_examples.tsv", nb_dev_accuracy)
    print("NB accuracy test_examples.tsv", nb_train_accuracy)
    
    dev_predicted_labels = nb.classify_many(dev_features_only)
    dev_confusion_matrix = nltk.ConfusionMatrix(dev_reference_labels, dev_predicted_labels)
    test_predicted_labels = nb.classify_many(test_features_only)
    test_confusion_matrix = nltk.ConfusionMatrix(test_reference_labels, test_predicted_labels)
    
    print("Confusion Matrix:", "dev_examples.tsv")
    print(str(dev_confusion_matrix))
    
    print("Confusion Matrix:", "test_examples.tsv")
    print(str(test_confusion_matrix))
    
    print("Training Bernoulli...")
    
    nb_sk = SklearnClassifier(BernoulliNB())
    nb_sk.train(train_data)
    nb_sk_dev_accuracy = nltk.classify.accuracy(nb_sk, features_data)
    nb_sk_train_accuracy = nltk.classify.accuracy(nb_sk, features_data_2)
    print("NB_SK accuracy dev_examples.tsv", nb_sk_dev_accuracy)
    print("NB_SK accuracy test_examples.tsv", nb_sk_train_accuracy)
    
    dev_predicted_labels = nb_sk.classify_many(dev_features_only)
    dev_confusion_matrix = nltk.ConfusionMatrix(dev_reference_labels, dev_predicted_labels)
    test_predicted_labels = nb_sk.classify_many(test_features_only)
    test_confusion_matrix = nltk.ConfusionMatrix(test_reference_labels, test_predicted_labels)
    
    print("Confusion Matrix:", "dev_examples.tsv")
    print(str(dev_confusion_matrix))
    
    print("Confusion Matrix:", "test_examples.tsv")
    print(str(test_confusion_matrix))
    
    
    print("Training DT...")
    dt_sk = SklearnClassifier(tree.DecisionTreeClassifier())
    dt_sk.train(train_data)
    dt_sk_dev_accuracy = nltk.classify.accuracy(dt_sk, features_data)
    dt_sk_train_accuracy = nltk.classify.accuracy(dt_sk, features_data_2)
    print("DT_SK accuracy dev_examples.tsv", dt_sk_dev_accuracy)
    print("DT_SK accuracy test_examples.tsv", dt_sk_train_accuracy)
    
    dev_predicted_labels = dt_sk.classify_many(dev_features_only)
    dev_confusion_matrix = nltk.ConfusionMatrix(dev_reference_labels, dev_predicted_labels)
    test_predicted_labels = dt_sk.classify_many(test_features_only)
    test_confusion_matrix = nltk.ConfusionMatrix(test_reference_labels, test_predicted_labels)
    
    print("Confusion Matrix:", "dev_examples.tsv")
    print(str(dev_confusion_matrix))
    
    print("Confusion Matrix:", "test_examples.tsv")
    print(str(test_confusion_matrix))
    
    
    print("Training SVM...")
    svm_c = SklearnClassifier(svm.SVC(gamma='auto'))
    svm_c.train(train_data)
    svm_c_dev_accuracy = nltk.classify.accuracy(svm_c, features_data)
    svm_c_train_accuracy = nltk.classify.accuracy(svm_c, features_data_2)
    print("SVM accuracy dev_examples.tsv", svm_c_dev_accuracy)
    print("SVM accuracy test_examples.tsv", svm_c_train_accuracy)
    
    positive_texts, negative_texts = data_helper.get_reviews(os.path.join(DATA_DIR, "train_examples.tsv"))
    category_texts = {"positive": positive_texts, "negative": negative_texts}
    pos_total_text = ""
    neg_total_text = ""
    for text in category_texts['positive']:
        pos_total_text += text
    for text in category_texts['negative']:
        neg_total_text += text
    
    pos_ef = get_word_embedding_features(pos_total_text)
    neg_ef = get_word_embedding_features(neg_total_text)
    train = [(pos_ef,'positive'),(neg_ef, 'negative')]
    
    svm_e = SklearnClassifier(svm.SVC(gamma='auto'))
    svm_e.train(train)
    svm_e_dev_accuracy = nltk.classify.accuracy(svm_e, features_data)
    svm_e_train_accuracy = nltk.classify.accuracy(svm_e, features_data_2)
    print("SVM embedding accuracy dev_examples.tsv", svm_e_dev_accuracy)
    print("SVM embedding accuracy test_examples.tsv", svm_e_train_accuracy)
    
        
    
def best_predict(review_file, dest_file):
    features_data, texts = build_features(review_file, "word_features")
    classifier = train_with_feature_selection(review_file, 'word_features', 'train', True)
    accuracy, cm = evaluate(classifier, features_data, texts)
    print("\nThe accuracy of {} is: {}".format(review_file, accuracy))
    print("Confusion Matrix:")
    print(str(cm))
    
    
    fout = open(dest_file, "w+", encoding="utf-8")
    features_only = []
    print("writing to file: ", dest_file)
    for feature_vectors, category in features_data:
        features_only.append(feature_vectors)
    predictions = classifier.classify_many(features_only)
    for prediction in predictions:
        fout.write(prediction + '\n')
    fout.close()

def competition_predict(review_file, dest_file):
    features_data, texts = build_features(review_file, "word_features")
    classifier = train_with_feature_selection(review_file, 'word_features', 'train', True)
    accuracy, cm = evaluate(classifier, features_data, texts)
    print("\nThe accuracy of {} is: {}".format(review_file, accuracy))
    print("Confusion Matrix:")
    print(str(cm))
    
    fout = open(dest_file, "w+", encoding="utf-8")
    features_only = []
    print("writing to file: ", dest_file)
    for feature_vectors, category in features_data:
        features_only.append(feature_vectors)
    predictions = classifier.classify_many(features_only)
    for prediction in predictions:
        fout.write(prediction + '\n')
    fout.close()
    



if __name__ == "__main__":
    """
    (a) The file with the reviews in it to be classified.
    (b) The second should be the name of a file to write predictions to. When saving predictions, each
    predicted label should be on a separate line in the output file, in the same order as the input file.
    This file should be the output of a function called evaluate. The evaluate function should also
    calculate the accuracy and confusion matrix if it is supplied with the example labels.
    """

    "data/test.txt"
    parser = argparse.ArgumentParser(description='Assignment 4')
    parser.add_argument('-r', dest="reviews", default=None, required=False,
                        help='The file with the reviews in it to be classified.')
    parser.add_argument('-p', dest="pred_file", default="predictions.txt", required=False,
                        help='The file to write predictions to.')
    parser.add_argument('-c', dest="type", default=None, required=False, help='specify what type of classifier you would like to use')

    args = parser.parse_args()
    
    if args.reviews is not None:
        # An example way of calling the script to make predictions
        # python3 classify.py -r data/test.txt -p test-preds.txt
        if args.type == "1.2":
            best_predict(args.reviews, args.pred_file)
        elif args.type == "2.4":
            competition_predict(args.reviews, args.pred_file)
    elif args.type == "scikit":
        train_all()      
    else:
        train_main()










