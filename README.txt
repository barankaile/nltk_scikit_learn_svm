README

Note: This program requires train_examples.tsv in the data directory to function.
	  It also requires dev_examples.tsv, and test_examples.tsv in the data directory for the scikit section to function

To run program for section 1.1:
	python3 restaurant-competition-p2.py
	
To run program for section 1.2:
	python3 restaurant-competition-p2.py -r filename.tsv -p outfilename.txt -c 1.2
	
To run program for scikit 2.1-2.2.2:
	python3 restaurant-competition-p2.py -c scikit

To run program for the competition:
	python3 restaurant-competition-p2.py -r filename.tsv -p outfilename.txt -c 2.4
